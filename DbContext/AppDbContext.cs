﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRegistration.DbContext
{
    public class AppDbContext : IdentityDbContext<AppUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityUser>().ToTable("User");

            builder.Entity<IdentityRole>().ToTable("Role");
            builder.Entity<IdentityUserClaim<String>>().ToTable("UserClaim");
            builder.Entity<IdentityUserRole<String>>().ToTable("UserRole");
            builder.Entity<IdentityUserLogin<String>>().ToTable("UserLogin");
            builder.Entity<IdentityRoleClaim<String>>().ToTable("RoleClaim");
            builder.Entity<IdentityUserToken<String>>().ToTable("UserToken");
           
        }
    }
}
