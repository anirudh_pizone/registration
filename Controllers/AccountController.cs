﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using UserRegistration.DbContext;
using UserRegistration.Models;

namespace UserRegistration.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]

    public class AccountController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }



        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody]Register register)
        {
            var user = new AppUser
            {
                UserName = register.Username,
                Firstname = register.Firstname,
                Lastname = register.Lastname,
                Email = register.Email
            };

            var results = await _userManager.CreateAsync(user, register.Password);
            if (!results.Succeeded)
            {
                return BadRequest(results.Errors);
            }

            //add a new claim
            var Claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,user.Id)
            };

            var SSKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("Auth").Get<AppSettings>().SecurityKey));
            var signInCredantials = new SigningCredentials(SSKey, SecurityAlgorithms.HmacSha256);
            //public string Register() { 
            var token = new JwtSecurityToken(signingCredentials: signInCredantials,claims:Claims);
            //return new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}